<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="CoolDownLine" version="0.2B" date="22/4/2016">
	<VersionSettings gameVersion="1.4.8" windowsVersion="1.0" savedVariablesVersion="1.0" /> 
     <Author name="Sullemunk" />
        <Description text="Adds Hotbar cooldowns on a timeline, Made By Sullemunk for RoR" />
        <Dependencies />
        <Files>
            <File name="CoolDownLine.lua" />
            <File name="CoolDownLine.xml" />
        </Files>
				<SavedVariables>		
			<SavedVariable name="CDLSAVES" />
			</SavedVariables>
        <OnInitialize>
            <CallFunction name="CoolDownLine.Initialize" />
        </OnInitialize>
        <OnUpdate>
		<CallFunction name="CoolDownLine.OnUpdate" />
    	  </OnUpdate>
        <OnShutdown />
    </UiMod>
</ModuleFile>