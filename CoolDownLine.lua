
CoolDownLine = {}
NextUp = 0

local Cools2={}
local GetHotbarData = GetHotbarData
local GetHotbarCooldown = GetHotbarCooldown

if not CDLSAVES then
CDLSAVES = {}
CDLSAVES.Offset = -50
end



function CoolDownLine.Initialize()
--Announce that the addon is loaded
	TextLogAddEntry("Chat", 0, L"<icon=57> CoolDownLine v0.2B Loaded")

--Make the main window/bar
	CreateWindow("CoolDownLineWnd", true)

--If abilitys are switched or annything change, update the hotbars and re-register all abilitys
	RegisterEventHandler(SystemData.Events.PLAYER_HOT_BAR_UPDATED,"CoolDownLine.RegisterAbilities")
	RegisterEventHandler(SystemData.Events.ENTER_WORLD,"CoolDownLine.RegisterAbilities")
	RegisterEventHandler(SystemData.Events.INTERFACE_RELOADED,"CoolDownLine.RegisterAbilities")
	RegisterEventHandler(SystemData.Events.LOADING_END,"CoolDownLine.RegisterAbilities")
	
--The texts marks
	LabelSetText("10secTimeValue",L"10s")	
	LabelSetText("30secTimeValue",L"30s")	
	LabelSetText("1minTimeValue",L"1m")	
	LabelSetText("10minTimeValue",L"10m")

--set the cooldown animations to 0 (stop them from animating)
Animationtimer = 0		
StartAnimation = 0	

end

function CoolDownLine.OnUpdate( timeElapsed )--this is whats running every frame

LowestCD = 9999		-- annything above this, i don't even bother showing
NextUp = nil		--next ability to be ready, start by setting it to nothing
DynamicImageSetTexture("CoolDownLineWndIcon","icon000020", 62, 62)		--sets the next-up-ability icon to empty
LabelSetText("NextCDValue",L"")		--clears the next-up-ability timer

for i=0,121	do		--goes throu every Hotbar slot, one at a time
	local windowName = "CoolDownLineWnd"..i		--assigns a window name to slots, "CoolDownLineWnd0" to "CoolDownLineWnd121"
	if GetHotbarCooldown(i) > 1.6 and GetHotbarCooldown(i) < 600 then --if the cooldown is above the Global cooldown (else it would show even abilitys w/o a cooldown)
		Cools2[i].ISONCOOLDOWN = true 		--set the hotbar slot to ISONCOOLDOWN if above statement is true..
	elseif GetHotbarCooldown(i) < 0.01 then	
		Cools2[i].ISONCOOLDOWN = false		--else ISONCOOLDOWN gets set to false..
		if ( DoesWindowExist( windowName ) ) then DestroyWindow(windowName);StartAnimation = 1 end	--and if it is false, destroy the window (or they will be stacking upp infinitly)
	end
	if Cools2[i].ISONCOOLDOWN == true then

		--Setting up the icon for ability with the Lowest cooldown on the Line
		if GetHotbarCooldown(i) < LowestCD then LowestCD = GetHotbarCooldown(i); NextUp = i end
		if NextUp ~= nil then
		local Nexticon = GetHotbarIcon(NextUp)	
		local Nexttime = TimeUtils.FormatTimeCondensed(GetHotbarCooldown(NextUp))
		LabelSetText("NextCDValue",towstring(Nexttime))
		if Nexticon < 10000 and Nexticon > 1000 then
		DynamicImageSetTexture("CoolDownLineWndIcon",tostring(L"icon00"..towstring(Nexticon)), 62, 62)	
		elseif Nexticon < 1000  and Nexticon > 100 then
		DynamicImageSetTexture("CoolDownLineWndIcon",tostring(L"icon000"..towstring(Nexticon)), 62, 62)
		elseif Nexticon < 100	then
		DynamicImageSetTexture("CoolDownLineWndIcon",tostring(L"icon0000"..towstring(Nexticon)), 62, 62)
		else 
		DynamicImageSetTexture("CoolDownLineWndIcon",tostring(L"icon0"..towstring(Nexticon)), 62, 62)			
		end		
	end
				
	if ( DoesWindowExist( windowName ) ) then 
	WindowSetShowing( windowName, true )  
	WindowSetOffsetFromParent(tostring(towstring(windowName)..L"BTimeValue"),0, 45+CDLSAVES.Offset) 	
	if GetHotbarCooldown(i) < 10 then
	WindowSetOffsetFromParent(windowName, math.floor(GetHotbarCooldown(i)*(28+(20-(GetHotbarCooldown(i)*2)))), -6) 
	LabelSetText(tostring(towstring(windowName)..L"BTimeValue"),wstring.format(L"%.01f",towstring(GetHotbarCooldown(i))))
	elseif GetHotbarCooldown(i) > 10 and GetHotbarCooldown(i) < 30 then
	WindowSetOffsetFromParent(windowName, 200+math.floor((GetHotbarCooldown(i)*8)), -6) 
local testtime = TimeUtils.FormatSeconds(GetHotbarCooldown(i), true)
	LabelSetText(tostring(towstring(windowName)..L"BTimeValue"),testtime)	
	elseif GetHotbarCooldown(i) > 30 and GetHotbarCooldown(i) < 60 then
	WindowSetOffsetFromParent(windowName, 320+math.floor((GetHotbarCooldown(i)*4)), -6)	
	local testtime = TimeUtils.FormatTimeCondensed(GetHotbarCooldown(i))
	LabelSetText(tostring(towstring(windowName)..L"BTimeValue"),testtime)	
	elseif GetHotbarCooldown(i) > 60 and GetHotbarCooldown(i) < 600 then
	WindowSetOffsetFromParent(windowName, 530+math.floor((GetHotbarCooldown(i)/2)), -6) 
	local testtime = TimeUtils.FormatTimeCondensed(GetHotbarCooldown(i))
	LabelSetText(tostring(towstring(windowName)..L"BTimeValue"),testtime)	
	end	
	else
	CreateWindowFromTemplate( windowName, "CoolDownLineICONTemplate", "CoolDownLineWnd" )
	WindowSetShowing( windowName, false ) 
	WindowSetParent(windowName,"CoolDownLineWnd")
	if GetHotbarIcon(i) < 10000 and GetHotbarIcon(i) > 1000 then
	DynamicImageSetTexture(tostring(towstring(windowName)..L"AIcon"),tostring(L"icon00"..towstring(GetHotbarIcon(i))), 62, 62)
	elseif GetHotbarIcon(i) < 1000 and GetHotbarIcon(i) > 100 then
	DynamicImageSetTexture(tostring(towstring(windowName)..L"AIcon"),tostring(L"icon000"..towstring(GetHotbarIcon(i))), 62, 62)
	elseif GetHotbarIcon(i) < 100 then
	DynamicImageSetTexture(tostring(towstring(windowName)..L"AIcon"),tostring(L"icon0000"..towstring(GetHotbarIcon(i))), 62, 62)	
	else
	DynamicImageSetTexture(tostring(towstring(windowName)..L"AIcon"),tostring(L"icon0"..towstring(GetHotbarIcon(i))), 62, 62)
	end
	end
end
end

if StartAnimation == 1 then
WindowSetShowing ("CoolDownLineWndFlash", true)
AnimatedImageStartAnimation ("CoolDownLineWndFlash", 0, true, true, 0)
StartAnimation = 0
Animationtimer = 0.6
end

	if Animationtimer > 0 then
	Animationtimer = Animationtimer - timeElapsed
	else
	AnimatedImageStopAnimation ("CoolDownLineWndFlash")
	end
end

function CoolDownLine.RegisterAbilities()
	local abilityData,ID,cooldown,TYPE
	Cools2={}
	for i=0,121	do
	Cools2[i] = {}
Cools2[i].ISONCOOLDOWN = false	
TYPE,ID=GetHotbarData(i)
abilityData=GetAbilityData(ID)
Cools2[i].ABDATA = abilityData
Cools2[i].ABID = ID
Cools2[i].ABTYPE = TYPE
end
end

